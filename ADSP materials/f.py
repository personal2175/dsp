import matplotlib.pyplot as plt
import numpy as np
from numpy import pi, sin, sinc, log10
from numpy.fft import fft, fftshift
from scipy.signal import hann

def make_hd(M, omega_c, window='rect'):
    L = 32
    n = np.arange(L)
    w = np.zeros(L)
    if window == 'rect':
        w[:M] = 1
    elif window == 'hann':
        w[:M] = hann(M, True)
    N = 1024
    W = fft(w, N)
    omega = np.linspace(-pi, pi, N)

    hd = (omega_c / pi) * sinc(omega_c * (n - (M-1)/2) / pi)

    h = w * hd
    H = fft(h, N)

    return omega, n, w, W, H

def e1():
    omega_c = pi / 4
    M = 9
    omega, n, w, W, H = make_hd(M, omega_c)
    HD = (abs(omega) <= omega_c).astype('float')

    _, axs = plt.subplots(2, 3, figsize=(16,10))
    axs[0,0].stem(n, w)
    axs[0,1].plot(omega, np.abs(fftshift(W)))
    axs[0,2].plot(omega, np.abs(fftshift(H)))
    axs[0,2].plot(omega, HD, color='r')
    axs[0,0].set_xlabel('$n$', fontsize=18)
    axs[0,0].set_ylim(-0.05, 1.1)
    axs[0,0].set_xlim(-1, 32)
    axs[0,0].set_title('Ventana rectangular, $M=%d$' % M)
    axs[0,1].set_xlabel(r'$\omega$', fontsize=18)
    axs[0,1].set_xlim(-pi, pi)
    axs[0,1].set_xticks(([-pi, -pi/2, 0, pi/2, pi]))
    axs[0,1].set_xticklabels((r'$-\pi$', r'$-\pi/2$', '0', r'$\pi/2$', r'$\pi$'), fontsize=18)
    axs[0,1].set_title('$W(\omega)$, $M=%d$' % M)
    axs[0,2].set_xlim(-pi, pi)
    axs[0,2].set_xticks(([-pi, -pi/2, 0, pi/2, pi]))
    axs[0,2].set_xticklabels((r'$-\pi$', r'$-\pi/2$', '0', r'$\pi/2$', r'$\pi$'), fontsize=18)
    axs[0,2].set_title('$H(\omega)$ y $H_d(\omega)$, $M=%d$' % M)

    M = 17
    omega, n, w, W, H = make_hd(M, omega_c)
    axs[1,0].stem(n, w)
    axs[1,1].plot(omega, np.abs(fftshift(W)))
    axs[1,2].plot(omega, np.abs(fftshift(H)))
    axs[1,2].plot(omega, HD, color='r')
    axs[1,0].set_xlabel('$n$', fontsize=18)
    axs[1,0].set_ylim(-0.05, 1.1)
    axs[1,0].set_xlim(-1, 32)
    axs[1,0].set_title('Ventana rectangular, $M=%d$' % M)
    axs[1,1].set_xlabel(r'$\omega$', fontsize=18)
    axs[1,1].set_xlim(-pi, pi)
    axs[1,1].set_xticks(([-pi, -pi/2, 0, pi/2, pi]))
    axs[1,1].set_xticklabels((r'$-\pi$', r'$-\pi/2$', '0', r'$\pi/2$', r'$\pi$'), fontsize=18)
    axs[1,1].set_title('$W(\omega)$, $M=%d$' % M)
    axs[1,2].set_xlim(-pi, pi)
    axs[1,2].set_xticks(([-pi, -pi/2, 0, pi/2, pi]))
    axs[1,2].set_xticklabels((r'$-\pi$', r'$-\pi/2$', '0', r'$\pi/2$', r'$\pi$'), fontsize=18)
    axs[1,2].set_title('$H(\omega)$ y $H_d(\omega)$, $M=%d$' % M)

    plt.show()

def e2():
    M = 9
    N = 20
    n = np.arange(N)
    w_rect = np.zeros(N)
    w_rect[:M] = 1
    w_hann_9 = np.zeros(N)
    w_hann_9[:M] = hann(M, True)
    M = 17
    w_hann_17 = np.zeros(N)
    w_hann_17[:M] = hann(M, True)
    W_rect = 20 * log10(np.abs(fftshift(fft(w_rect, 1024))))
    tmp = fft(w_hann_9, 1024)
    tmp[tmp == 0] = 1e-16 # To avoid dividing by zero when computing dB
    W_hann_9 = 20 * log10(np.abs(fftshift(tmp)))
    tmp = fft(w_hann_17, 1024)
    tmp[tmp == 0] = 1e-16 # To avoid dividing by zero when computing dB
    W_hann_17 = 20 * log10(np.abs(fftshift(tmp)))

    _, axs = plt.subplots(3,2, figsize=(16, 10))
    axs[0,0].stem(n, w_rect)
    axs[1,0].stem(n, w_hann_9)
    axs[2,0].stem(n, w_hann_17)

    omega = np.linspace(-pi, pi, 1024)
    axs[0,1].plot(omega, W_rect)
    axs[1,1].plot(omega, W_hann_9)
    axs[2,1].plot(omega, W_hann_17)

    for i, M, t in zip(range(3), [9, 9, 17], ['rectangular', 'hann', 'hann']):
        axs[i,1].set_ylim(-60, 20)
        axs[i,0].set_xlim(-1, N)
        axs[i,0].set_ylim(-0.05, 1.1)

        axs[i,1].set_xlim(-pi, pi)
        axs[i,1].set_xticks(([-pi, -pi/2, 0, pi/2, pi]))
        axs[i,1].set_xticklabels((r'$-\pi$', r'$-\pi/2$', '0', r'$\pi/2$', r'$\pi$'), fontsize=18)
        axs[i,1].set_title('$W(\omega)$ ventana %s, $M=%d$' % (t, M))
        axs[i,0].set_title('$w(n)$ ventana %s, $M=%d$' % (t, M))


    axs[2,1].set_xlabel(r'$\omega$', fontsize=18)
    axs[2,0].set_xlabel('$n$', fontsize=18)
    plt.show()


omega_c = pi / 4
M = 9
omega, n, w, W_rect, H = make_hd(M, omega_c, 'rect')
omega, n, w, W_hann_9, H = make_hd(9, omega_c, 'hann')
omega, n, w, W_hann_17, H = make_hd(17, omega_c, 'hann')

_, axs = plt.subplots(3, 1, figsize=(8,10))

axs[0].plot(omega, 20*log10(np.abs(fftshift(W_rect))))
W_hann_9[W_hann_9 == 0] = 1e-16
W_hann_17[W_hann_17 == 0] = 1e-16
axs[1].plot(omega, 20*log10(np.abs(fftshift(W_hann_9))))
axs[2].plot(omega, 20*log10(np.abs(fftshift(W_hann_17))))

for i, M, t in zip(range(3), [9, 9, 17], ['rectangular', 'hann', 'hann']):
    axs[i].set_ylim(-60, 20)

    axs[i].set_xlim(-pi, pi)
    axs[i].set_xticks(([-pi, -pi/2, 0, pi/2, pi]))
    axs[i].set_xticklabels((r'$-\pi$', r'$-\pi/2$', '0', r'$\pi/2$', r'$\pi$'), fontsize=18)
    axs[i].set_title('$H(\omega)$ ventana %s, $M=%d$' % (t, M))

axs[2].set_xlabel('$\omega$', fontsize=18)

plt.show()
